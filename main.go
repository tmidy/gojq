package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/itchyny/gojq"
)

func executeCommand(cmd *string, in *string, out *string) {
	jsonFile, err := os.Open(*in)
	if err != nil {
		fmt.Println(err)
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var jsonInterface interface{}
	json.Unmarshal(byteValue, &jsonInterface)

	// query
	query, err := gojq.Parse(*cmd)
	if err != nil {
		log.Fatalln(err)
	}

	defer jsonFile.Close()

	// check de la query
	iter := query.Run(jsonInterface)
	v, ok := iter.Next()
	if !ok {
		log.Fatalln(ok)
	}
	str, err := gojq.Marshal(v)
	if err != nil {
		fmt.Print(cmd)
	}

	if out == nil || len(*out) == 0 {
		fmt.Print(string(str))
		return
	}

	f, err := os.Create(*out)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	_, err = f.WriteString(string(str))
	if err != nil {
		log.Fatal(err)
	}

}

func main() {

	cmd := flag.String("cmd", "", "jq query")
	in := flag.String("input", "", "inputfile")
	out := flag.String("output", "", "outputfile")

	flag.Parse()
	if cmd == nil || in == nil || len(*cmd) == 0 || len(*in) == 0 {
		log.Fatal("pas assez d'arguments")
	}
	executeCommand(cmd, in, out)
}
