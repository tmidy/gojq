# gojq

Utilitaire pour lancer des commandes jq

### Compilation

```bash
#ARM
env CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ CGO_ENABLED=1 GOOS=linux GOARCH=arm GOARM=7 go build -o gojq-arm

# Linux
env GOOS=linux GOARCH=amd64  /usr/local/go/bin/go build -o gojq-linux

#Mac
??
```

## lancement

```bash
go run main.go -cmd=[COMMAND_JQ] -input=[INPUT_FILE] -output=[OUTPUT_FILE]

OU
# Si vous avez buildé le binaire
gojq-arm -cmd=[COMMAND_JQ] -input=[INPUT_FILE] -output=[OUTPUT_FILE]
```

L'output est optionnel.

## Exemple

```json
[
  { "name": "JSON", "good": true },
  { "name": "XML", "good": false }
]
```

```bash
go run main.go --cmd=.[1] --input=input.json
```

```json
{
  "name": "XML",
  "good": false
}
```
